<?php
/*
 yr.php  -  YR.no forecast on YOUR page!

 This script was downloaded from http://www.yr.no/verdata/1.5542682
 New page: http://om.yr.no/verdata/php/
 Please read the tips on that page on how you would/should use this script

 You need a webserver with PHP version 5 or later to run this script.
 A lot of comments are in Norwegian only. We will be translating to english whenever we have the opportunity.
 For feedback / bug repports / feature requests, please contact us: http://om.yr.no/sporsmal/kontakt-yr-no

 ###### Changelog

 Version 3.1 - Andreas Røste (andreas200@live.no) 2016.08.15 16.45
 * Fixed multiple declaretions in generateHTMLCached.
 * Corrected link to copyright.
 * Small bugfixes. This API now fully works with PHP 7.1!

 Version: 3.0 - Marius Undrum (marius.undrum@nrk.no) / NRK 2015.09.02
 * Changed encoding from ISO-8859-1 to UTF-8
 * Updated css url
 * Updated weather symbols url

 Versjon: 2.6 - Lennart André Rolland (lennart.andre.rolland@nrk.no) / NRK - 2008.11.11 11:48
 * Added option to remove banner ($yr_use_banner)
 * Added option to allow any target for yr.no urls ($yr_link_target)

 Versjon: 2.5 - Lennart André Rolland (lennart.andre.rolland@nrk.no) / NRK - 2008.09.25 09:24
 * Cache will now update on parameter changes (cache file is prefixed with md5 digest of all relevant parameters)
   This change will in the future make it easier to use the script for multiple locations in one go.
 * Most relevant comments translated to english

Version 2.4 - Sven-Ove Bjerkan (sven-ove@smart-media.no) / Smart-Media AS - 2008.10.22 12:14
  * Changed functionality ifbm with displaying PHP errors (removed including all "@", this is controlled by error_reporting ())
  * When error message, it was stored in local cache so that it received error message every time until "$ yr_maxage" occurs and it tries to reload - it does not cache now if an error occurs
  * $ yr_use_text, $ yr_use_links and $ yr_use_table were overridden to "true" regardless of the user's setting - fixed!

  Version: 2.3 - Lennart André Rolland (lennart.andre.rolland@nrk.no) / NRK - 2008.09.25 09:24
  * File permissions updated
  * Caching is stored in HTML isntead or XML for security
  * Other security and efficiency improvements

 ###### INSTRUCTIONS:

 1. Edit this script in editors with UTF-8 character set.
 2. Edit the settings below
 3. Transfer the script to a folder in your webroot.
 4. Make sure that the webserver has write access to the folder where thsi script is placed. It will create a folder called yr-cache and place cached HTML data in that directory.

 */

///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  /
///  ///  ///  ///  ///  Settings  ///  ///  ///  ///  ///  ///  ///  ///  //
//  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///

// 1. Link: Link to the site at yr.no (Without the last slash.
// Link: Link to the url for the location on yr.no (Without the last Slash.)
$yr_url = 'https://www.yr.no/sted/Norge/Buskerud/Ringerike/Hønefoss';

// 2. Place name: Enter the name of the place. Leave empty to fall back to the name in the link
// Location: The name of the location. Leave empty to fallback to the location in the url.
$yr_name = 'Hønefoss';

// 3. Use header and footer: Choose if you want to include header and / or footer
// Use Header and footers: Select to have HTML headers / footers wrapping the content (useful for debugging)
// PS: The header for the HTML document is XHTML 1.0 Strict
// Shut off usually when you include in an existing document!
//
$yr_use_header = $yr_use_footer = true;

// 4. Parts: Select the parts of the alert you want to include!
// Parts: Choose which parts of the forecast to include
$yr_use_banner = true; //yr.no Banner
$yr_use_text = false; // Text forecast
$yr_use_links = true; // Links to alert on yr.no
$yr_use_table = true; // The table with the alert

// 5. Intermediate storage time: Number of seconds before new notification is retrieved from yr.no.
// Cachetime: Number of seconds to keep forecast in local cache
// The recommended value of 1200 will update the page every 20 minutes.
//
// PS: We want you to set 20 minutes of intermediate storage time because
// it will give higher performance, both for yr.no and you! BUT to get to this
// we will create a folder and save a file in that folder. We've gone
// through the script very carefully to make sure it is flawless.
// Nevertheless, this is not completely unproblematic in terms of security.
// If you have problems with this, you can set $ yr_maxage to 0 to turn on
// of middle storage completely!
$yr_maxage = 1200;

// 6. Expiration time: This setting allows you to choose how long yr.no has for
// deliver the alert in seconds.
// Timeout: How long before this script gives up fetching data from yr.no
//
// If yr.no should be down or it is
// bandwidth interruptions otherwise, the alert will be replaced with one
// error message to the situation is improved again. PS: only applies when new
// alert fetched! Does not affect notification while the page displays notification from
// middle storage. The recommended value of 10 seconds works well.
$yr_timeout = 10;

// 7. Intermediate storage folder: Select the name of the intermediate storage data folder.
// Cache folder: Where to put cache data
//
// This script will try to create the folder if it does not exist.
$yr_datadir = 'yr_cache';


// 8. Link goals: Choose which target to use for links to yr.no
// Link target: Choose which target to use for links to yr.no
$yr_link_target = '_top';

// 9. View error messages: Set to "true" for error messages.
// Show errors: Useful while debugging.
//
// fine for troubleshooting, but should not be enabled in operation.
$yr_vis_php_feilmeldinger = true;















///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  /
///  ///  ///  ///  ///  Code ///  ///  ///  ///  ///  ///  ///  ///  ///  //
//  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///
// Turn on error messages at first
if ($yr_vis_php_feilmeldinger) {
    error_reporting(E_ALL);
    ini_set('display_errors', true);
} else {
    error_reporting(0);
    ini_set('display_errors', false);
}

// Create a communication with yr
$yr_xmlparse = new YRComms();
// Create a presentation
$yr_xmldisplay = new YRDisplay();

$yr_try_curl = true;

// Execute the mission basta boom.
die($yr_xmldisplay->generateHTMLCached($yr_url, $yr_name, $yr_xmlparse, $yr_try_curl, $yr_use_header, $yr_use_footer, $yr_use_banner, $yr_use_text, $yr_use_links, $yr_use_table, $yr_maxage, $yr_timeout, $yr_link_target));


///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  /
///  ///  ///  ///  ///  Help code starts here   ///  ///  ///  ///  ///  //
//  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///  ///


function retar($array, $html = false, $level = 0)
{
    if (is_array($array)) {
        $space = $html ? "&nbsp;" : " ";
        $newline = $html ? "<br />" : "\n";
        $spaces = '';
        for ($i = 1; $i <= 3; $i++) $spaces .= $space;
        $tabs = $spaces;
        for ($i = 1; $i <= $level; $i++) $tabs .= $spaces;
        $output = "Array(" . $newline . $newline;
        $cnt = sizeof($array);
        $j = 0;
        foreach ($array as $key => $value) {
            $j++;
            if (is_array($value)) {
                $level++;
                $value = retar($value, $html, $level);
                $level--;
            } else $value = "'$value'";
            $output .=  "$tabs'$key'=> $value";
            if ($j < $cnt) $output .=  ',';
            $output .=  $newline;
        }
        $output .= $tabs . ')' . $newline;
    } else {
        $output = "'$array'";
    }
    return $output;
}


// Class for reading and arranging YR data
class YRComms
{

    // Generate valid yr.no array with weather data replaced with a simple error message
    private function getYrDataErrorMessage($msg = "Feil")
    {
        return array(
            '0' => array('tag' => 'WEATHERDATA', 'type' => 'open', 'level' => '1'),
            '1' => array('tag' => 'LOCATION', 'type' => 'open', 'level' => '2'),
            '2' => array('tag' => 'NAME', 'type' => 'complete', 'level' => '3', 'value' => $msg),
            '3' => array('tag' => 'LOCATION', 'type' => 'complete', 'level' => '3'),
            '4' => array('tag' => 'LOCATION', 'type' => 'close', 'level' => '2'),
            '5' => array('tag' => 'FORECAST', 'type' => 'open', 'level' => '2'),
            '6' => array('tag' => 'ERROR', 'type' => 'complete', 'level' => '3', 'value' => $msg),
            '7' => array('tag' => 'FORECAST', 'type' => 'close', 'level' => '2'),
            '8' => array('tag' => 'WEATHERDATA', 'type' => 'close', 'level' => '1')
        );
    }

    // Generate valid yr.no XML with weather data replaced with a simple error message
    private function getYrXMLErrorMessage($msg = "Feil")
    {
        $msg = $this->getXMLEntities($msg);
        //die('errmsg:'.$msg);
        $data = <<<EOT
<weatherdata>
  <location />
  <forecast>
  <error>$msg</error>
    <text>
      <location />
    </text>
  </forecast>
</weatherdata>

EOT;
        //die($data);
        return $data;
    }

    // Ensures to download XML from yr.no and returns data in a string
    private function loadXMLData($xml_url, $try_curl = true, $timeout = 10)
    {
        global $yr_datadir;
        $xml_url .= '/varsel.xml';
        // Create a timeout on the context
        $ctx = stream_context_create(array('http' => array('timeout' => $timeout)));

        // Try opening directly first
        //NOTE: This will spew ugly errors even when they are handled later. There is no way to avoid this but prefixing with @ (slow) or turning off error reporting
        $data = file_get_contents($xml_url, 0, $ctx);

        if (false != $data) {
            // Jippi we managed it with regular fopen url wrappers!
        }
        // Common fopen_wrapper failed, but we have cURL available
        else if ($try_curl && function_exists('curl_init')) {
            $lokal_xml_url = $yr_datadir . '/curl.temp.xml';
            $data = '';
            $ch = curl_init($xml_url);
            // Open the local temp file for write access (with cURL hooks enabled)
            $fp = fopen($lokal_xml_url, "w");
            // Load from yr.no to local copy with curl
            curl_setopt($ch, CURLOPT_FILE, $fp);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_POSTFIELDS, '');
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
            curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
            curl_exec($ch);
            curl_close($ch);
            // Close local copy
            fclose($fp);
            // Open local copy again and read all the content
            $data = file_get_contents($lokal_xml_url, 0, $ctx);
            // Delete temp data
            unlink($lokal_xml_url);
            // Check for errors
            if (false == $data) $data = $this->getYrXMLErrorMessage('An error occurred while reading weather data from yr.no. Technical info: Most likely: link failed. Second most likely: The fopen wrapper is missing support, and cURL also failed. Least presumably: cURL does not have permission to store temp.xml');
        }
        // We have neither fopen_wrappers nor cURL
        else {
            $data = $this->getYrXMLErrorMessage('An error occurred while trying to read weather data from yr.no. Technical info: This PHP installation has neither URL enabling fopen_wrappers nor cURL. This makes it impossible to retrieve weather data. However, see the following documentation: http://no.php.net/manual/en/wrappers.php, http://no.php.net/manual/en/book.curl.php');
            //die('<pre>LO:'.retar($data));
        }
        //die('<pre>XML for:'.$xml_url.' WAS: '.$data);
        // Once we get here, there is some indication that we have succeeded in loading weather data, or at least create an email message describing any problems
        return $data;
    }

    // Load XML to an array structure
    private function parseXMLIntoStruct($data)
    {
        global $yr_datadir;
        $parser = xml_parser_create('ISO-8859-1');
        if ((0 == $parser) || (FALSE == $parser)) return $this->getYrDataErrorMessage('An error occurred while trying to retrieve weather data from yr.no. Technical info: Failed to create XML parser.');
        $vals = array();
        //die('<pre>'.retar($data).'</pre>');
        if (FALSE == xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1)) return $this->getYrDataErrorMessage('An error occurred while trying to retrieve weather data from yr.no. Technical info: Could not set XML parser.');
        if (0 == xml_parse_into_struct($parser, $data, $vals, $index)) return $this->getYrDataErrorMessage('An error occurred while trying to retrieve weather data from yr.no. Technical info: XML parsing failed.');
        if (FALSE == xml_parser_free($parser)) return $this->getYrDataErrorMessage('An error occurred while trying to retrieve weather data from yr.no. Failed to release XML parser.');
        //die('<pre>'.retar($vals).'</pre>');
        return $vals;
    }


    // Clear text data (for security reasons)
    private function sanitizeString($in)
    {
        //return $in;
        if (is_array($in)) return $in;
        if (null == $in) return null;
        return htmlentities(strip_tags($in));
    }

    // Clear text data (for security reasons)
    public function reviveSafeTags($in)
    {
        //$in=$in.'<strong>STRONG</strong> <u>UNDERLINE</u> <b>BOLD</b> <i>ITALICS</i>';
        return str_ireplace(array('&lt;strong&gt;', '&lt;/strong&gt;', '&lt;u&gt;', '&lt;/u&gt;', '&lt;b&gt;', '&lt;/b&gt;', '&lt;i&gt;', '&lt;/i&gt;'), array('<strong>', '</strong>', '<u>', '</u>', '<b>', '</b>', '<i>', '</i>'), $in);
    }



    private function rearrangeChildren($vals, &$i)
    {
        $children = array(); // Contains node data
        // Security: make sure all data being parsed is stripped of dangerous things
        if (isset($vals[$i]['value'])) $children['VALUE'] = $this->sanitizeString($vals[$i]['value']);
        while (++$i < count($vals)) {
            // Security: make sure all data being parsed is stripped of dangerous things
            if (isset($vals[$i]['value'])) $val = $this->sanitizeString($vals[$i]['value']);
            else unset($val);
            if (isset($vals[$i]['type'])) $typ = $this->sanitizeString($vals[$i]['type']);
            else unset($typ);
            if (isset($vals[$i]['attributes'])) $atr = $this->sanitizeString($vals[$i]['attributes']);
            else unset($atr);
            if (isset($vals[$i]['tag'])) $tag = $this->sanitizeString($vals[$i]['tag']);
            else unset($tag);
            // Fill in the structure please as we want it
            switch ($vals[$i]['type']) {
                case 'cdata':
                    $children['VALUE'] = (isset($children['VALUE'])) ? $val : $children['VALUE'] . $val;
                    break;
                case 'complete':
                    if (isset($atr)) {
                        $children[$tag][]['ATTRIBUTES'] = $atr;
                        $index = count($children[$tag]) - 1;
                        if (isset($val)) $children[$tag][$index]['VALUE'] = $val;
                        else $children[$tag][$index]['VALUE'] = '';
                    } else {
                        if (isset($val)) $children[$tag][]['VALUE'] = $val;
                        else $children[$tag][]['VALUE'] = '';
                    }
                    break;
                case 'open':
                    if (isset($atr)) {
                        $children[$tag][]['ATTRIBUTES'] = $atr;
                        $index = count($children[$tag]) - 1;
                        $children[$tag][$index] = array_merge($children[$tag][$index], $this->rearrangeChildren($vals, $i));
                    } else $children[$tag][] = $this->rearrangeChildren($vals, $i);
                    break;
                case 'close':
                    return $children;
            }
        }
    }
    // Re-furnish data to fit our purpose and return
    private function rearrangeDataStruct($vals)
    {
        //die('<pre>'.$this->retar($vals).'<\pre>');
        $tree = array();
        $i = 0;
        if (isset($vals[$i]['attributes'])) {
            $tree[$vals[$i]['tag']][]['ATTRIBUTES'] = $vals[$i]['attributes'];
            $index = count($tree[$vals[$i]['tag']]) - 1;
            $tree[$vals[$i]['tag']][$index] = array_merge($tree[$vals[$i]['tag']][$index], $this->rearrangeChildren($vals, $i));
        } else $tree[$vals[$i]['tag']][] = $this->rearrangeChildren($vals, $i);
        //die("<pre>".retar($tree));
        // Get out what we care about
        if (isset($tree['WEATHERDATA'][0]['FORECAST'][0])) return $tree['WEATHERDATA'][0]['FORECAST'][0];
        else return YrComms::getYrDataErrorMessage('There was an error processing data from yr.no. Please make the administrator aware of this! Technical: Data is incorrectly formatted.');
    }

    // Main method. Loading XML from a yr.no URI and parsing it
    public function getXMLTree($xml_url, $try_curl, $timeout)
    {
        // Load XML file and parse into an array hierarchy, reshuffle data to fit our purpose, and return
        return $this->rearrangeDataStruct($this->parseXMLIntoStruct($this->loadXMLData($xml_url, $try_curl, $timeout)));
    }

    // Static helps to parse time in yr format
    public static function parseTime($yr_time, $do24_00 = false)
    {
        $yr_time = str_replace(":00:00", "", $yr_time);
        if ($do24_00) $yr_time = str_replace("00", "24", $yr_time);
        return $yr_time;
    }

    // Static helps provide proper encoding by translating special ISO-8859-1 characters into HTML / XHTML entities
    public static function convertEncodingEntities($yrraw)
    {
        $conv = str_replace("æ", "&aelig;", $yrraw);
        $conv = str_replace("ø", "&oslash;", $conv);
        $conv = str_replace("å", "&aring;", $conv);
        $conv = str_replace("Æ", "&AElig;", $conv);
        $conv = str_replace("Ø", "&Oslash;", $conv);
        $conv = str_replace("Å", "&Aring;", $conv);
        return $conv;
    }

    // Static helps to provide proper encoding by translating special UTF characters to ISO-8859-1
    public static function convertEncodingUTF($yrraw)
    {
        $conv = str_replace("Ã¦", "æ", $yrraw);
        $conv = str_replace("Ã¸", "ø", $conv);
        $conv = str_replace("Ã¥", "å", $conv);
        $conv = str_replace("Ã", "Æ", $conv);
        $conv = str_replace("Ã", "Ø", $conv);
        $conv = str_replace("Ã", "Å", $conv);
        return $conv;
    }


    public function getXMLEntities($string)
    {
        return preg_replace('/[^\x09\x0A\x0D\x20-\x7F]/e', '$this->_privateXMLEntities("$0")', $string);
    }

    private function _privateXMLEntities($num)
    {
        $chars = array(
            128 => '&#8364;', 130 => '&#8218;',
            131 => '&#402;', 132 => '&#8222;',
            133 => '&#8230;', 134 => '&#8224;',
            135 => '&#8225;', 136 => '&#710;',
            137 => '&#8240;', 138 => '&#352;',
            139 => '&#8249;', 140 => '&#338;',
            142 => '&#381;', 145 => '&#8216;',
            146 => '&#8217;', 147 => '&#8220;',
            148 => '&#8221;', 149 => '&#8226;',
            150 => '&#8211;', 151 => '&#8212;',
            152 => '&#732;', 153 => '&#8482;',
            154 => '&#353;', 155 => '&#8250;',
            156 => '&#339;', 158 => '&#382;',
            159 => '&#376;'
        );
        $num = ord($num);
        return (($num > 127 && $num < 160) ? $chars[$num] : "&#" . $num . ";");
    }
}

// Class to display data from yr. Compatible with YRComm's data structure
class YRDisplay
{

    // Accumulator variable to hold on generated HTML
    var $ht = '';
    // Yr Url
    var $yr_url = '';
    // Yr city name
    var $yr_name = '';
    // Yr data
    var $yr_data = array();

    //Filename for cached HTML. MD5 hash will be prepended to allow caching of several pages
    var $datafile = 'yr.html';
    //The complete path to the cache file
    var $datapath = '';

    // Norwegian coarse distribution of the 360 degree wind direction
    var $yr_vindrettninger = array(
        'nord', 'nord-nord&oslash;st', 'nord&oslash;st', '&oslash;st-nord&oslash;st',
        '&oslash;st', '&oslash;st-s&oslash;r&oslash;st', 's&oslash;r&oslash;st', 's&oslash;r-s&oslash;r&oslash;st',
        's&oslash;r', 's&oslash;r-s&oslash;rvest', 's&oslash;rvest', 'vest-s&oslash;rvest',
        'vest', 'vest-nordvest', 'nordvest', 'nord-nordvest', 'nord'
    );

    // Where do images for symbols come from?
    var $yr_imgpath = 'https://www.yr.no/grafikk/sym/b38';


    // Generate header for the alert
    public function getHeader($use_full_html)
    {
        // Here you can change the header to whatever you want. NB! Remember to turn it on by changing the settings at the top of the document
        if ($use_full_html) {
            $this->ht .= <<<EOT
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>V&aelig;rvarsel fra yr.no</title>
    <link href="css/yr-php.css" rel="stylesheet" type="text/css" />
  </head>
  <body>

EOT;
        }
        $this->ht .= <<<EOT
    <div id="yr-varsel">

EOT;
    }

    // Generate footer for the alert
    public function getFooter($use_full_html)
    {
        $this->ht .= <<<EOT
    </div>

EOT;
        // Here you can change the footer to whatever you want. NB! Remember to turn it on by changing the settings at the top of the document
        if ($use_full_html) {
            $this->ht .= <<<EOT
  </body>
</html>

EOT;
        }
    }


    // Generate Copyright for data from yr.no
    public function getBanner($target = '_top')
    {
        $url = YRComms::convertEncodingEntities($this->yr_url);
        $this->ht .= <<<EOT
      <h1><a href="https://www.yr.no/" target="$target"><img src="https://www.yr.no/grafikk/sym/php-varsel/topp.png" alt="yr.no" title="yr.no is a service from the Meteorological Institute and NRK" /></a></h1>

EOT;
    }


    // Generate Copyright for data from yr.no
    public function getCopyright($target = '_top')
    {
        $url = YRComms::convertEncodingEntities($this->yr_url);
        /*
          You must include the text below and include a link to yr.no.
          By removing this text and links, you are violating the terms of use of data from yr.no.
          It is a criminal offense to use data from yr.no in violation of the terms.
          You can find the terms at http://om.yr.no/verdata/vilkar/         */
        $this->ht .= <<<EOT
      <h2><a href="$url" target="$target">V&aelig;rvarsel for $this->yr_name</a></h2>
      <p><a href="https://www.yr.no/" target="$target"><strong>Warning from yr.no, provided by the Norwegian Meteorological Institute and NRK.</strong></a></p>

EOT;
    }


    // Generate text for weather
    public function getWeatherText()
    {
        if ((isset($this->yr_data['TEXT'])) && (isset($this->yr_data['TEXT'][0]['LOCATION'])) && (isset($this->yr_data['TEXT'][0]['LOCATION'][0]['ATTRIBUTES']))) {
            $yr_place = $this->yr_data['TEXT'][0]['LOCATION'][0]['ATTRIBUTES']['NAME'];
            if (!isset($this->yr_data['TEXT'][0]['LOCATION'][0]['TIME'])) return;
            foreach ($this->yr_data['TEXT'][0]['LOCATION'][0]['TIME'] as $yr_var2) {
                // Small letters
                $l = (YRComms::convertEncodingUTF($yr_var2['TITLE'][0]['VALUE']));
                // Fixed encoding
                $e = YRComms::reviveSafeTags(YRComms::convertEncodingUTF($yr_var2['BODY'][0]['VALUE']));
                // Spit out!
                $this->ht .= <<<EOT
      <p><strong>$yr_place $l</strong>:$e</p>

EOT;
            }
        }
    }

    // Generate links to other alerts
    public function getLinks($target = '_top')
    {
        // Clean url
        $url = YRComms::convertEncodingEntities($this->yr_url);
        // Spit out
        $this->ht .= <<<EOT
      <p class="yr-lenker">$this->yr_name p&aring; yr.no:
        <a href="$url/" target="$target">Varsel med kart</a>
        <a href="$url/time_for_time.html" target="$target">Time for time</a>
        <a href="$url/helg.html" target="$target">Helg</a>
        <a href="$url/langtidsvarsel.html" target="$target">Langtidsvarsel</a>
      </p>

EOT;
    }

    // Generate header for the weather data table
    public function getWeatherTableHeader()
    {
        $name = $this->yr_name;
        $this->ht .= <<<EOT
      <table summary="V&aelig;rvarsel for $name fra yr.no">
        <thead>
          <tr>
            <th class="v" colspan="3"><strong>Varsel for $name</strong></th>
            <th>Nedb&oslash;r</th>
            <th>Temp.</th>
            <th class="v">Vind</th>
            <th>Vindstyrke</th>
          </tr>
        </thead>
        <tbody>

EOT;
    }


    // Generate the contents of the weather data table
    public function getWeatherTableContent()
    {
        $thisdate = '';
        $dayctr = 0;
        if (!isset($this->yr_data['TABULAR'][0]['TIME'])) return;
        $a = $this->yr_data['TABULAR'][0]['TIME'];

        foreach ($a as $yr_var3) {
            list($fromdate, $fromtime) = explode('T', $yr_var3['ATTRIBUTES']['FROM']);
            list($todate, $totime) = explode('T', $yr_var3['ATTRIBUTES']['TO']);
            $fromtime = YRComms::parseTime($fromtime);
            $totime = YRComms::parseTime($totime, 1);
            if ($fromdate != $thisdate) {
                $divider = <<<EOT
          <tr>
            <td colspan="7" class="skilje"></td>
          </tr>

EOT;
                list($thisyear, $thismonth, $thisdate) = explode('-', $fromdate);
                $displaydate = $thisdate . "." . $thismonth . "." . $thisyear;
                $firstcellcont = $displaydate;
                $thisdate = $fromdate;
                ++$dayctr;
            } else $divider = $firstcellcont = '';

            // View new date
            if ($dayctr < 7) {
                $this->ht .= $divider;
                // Process symbol
                $imgno = $yr_var3['SYMBOL'][0]['ATTRIBUTES']['NUMBER'];
                if ($imgno < 10) $imgno = '0' . $imgno;
                switch ($imgno) {
                    case '01':
                    case '02':
                    case '03':
                    case '05':
                    case '06':
                    case '07':
                    case '08':
                        $imgno .= "d";
                        $do_daynight = 1;
                        break;
                    default:
                        $do_daynight = 0;
                }
                // Process rain
                $rain = $yr_var3['PRECIPITATION'][0]['ATTRIBUTES']['VALUE'];
                if ($rain == 0.0) $rain = "0";
                else {
                    $rain = intval($rain);
                    if ($rain < 1) $rain = '&lt;1';
                    else $rain = round($rain);
                }
                $rain .= " mm";
                // Process wind
                $winddir = round($yr_var3['WINDDIRECTION'][0]['ATTRIBUTES']['DEG'] / 22.5);
                $winddirtext = $this->yr_vindrettninger[$winddir];
                // Process temperature
                $temper = round($yr_var3['TEMPERATURE'][0]['ATTRIBUTES']['VALUE']);
                if ($temper >= 0) $tempclass = 'pluss';
                else $tempclass = 'minus';

                // Round of wind speed
                $r = round($yr_var3['WINDSPEED'][0]['ATTRIBUTES']['MPS']);
                // Then we put out the entire finished line
                $s = $yr_var3['SYMBOL'][0]['ATTRIBUTES']['NAME'];
                $w = $yr_var3['WINDSPEED'][0]['ATTRIBUTES']['NAME'];

                $this->ht .= <<<EOT
          <tr>
            <th>$firstcellcont</th>
            <th>$fromtime&#8211;$totime</th>
            <td><img src="$this->yr_imgpath/$imgno.png" width="38" height="38" alt="$s" /></td>
            <td>$rain</td>
            <td class="$tempclass">$temper&deg;</td>
            <td class="v">$w fra $winddirtext</td>
            <td>$r m/s</td>
          </tr>

EOT;
            }
        }
    }

    // Generate footer for the weather data table
    public function getWeatherTableFooter($target = '_top')
    {
        $this->ht .= <<<EOT
          <tr>
            <td colspan="7" class="skilje"></td>
          </tr>
        </tbody>
      </table>
      <p>V&aelig;rsymbolet og nedb&oslash;rsvarselet gjelder for hele perioden, temperatur- og vindvarselet er for det f&oslash;rste tidspunktet. &lt;1 mm betyr at det vil komme mellom 0,1 og 0,9 mm nedb&oslash;r.<br />
      <a href="http://www.yr.no/1.3362862" target="$target">Slik forst&aring;r du varslene fra yr.no</a>.</p>
      <p>Vil du ogs&aring; ha <a href="http://www.yr.no/verdata/" target="$target">v&aelig;rvarsel fra yr.no p&aring; dine nettsider</a>?</p>
EOT;
    }


    // Handle cache directory (re) creation and cachefile name selection
    private function handleDataDir($clean_datadir = false, $summary = '')
    {
        global $yr_datadir;
        // The md5 sum is to avoid caching to the same file on parameter changes
        $this->datapath = $yr_datadir . '/' . ($summary != '' ? (md5($summary) . '[' . $summary . ']_') : '') . $this->datafile;
        // Delete cache dir
        if ($clean_datadir) {
            unlink($this->datapath);
            rmdir($yr_datadir);
        }
        // Create new cache folder with correct permissions
        if (!is_dir($yr_datadir)) mkdir($yr_datadir, 0300);
    }


    //Main with caching
    public function generateHTMLCached($url, $name, $xml, $try_curl, $useHtmlHeader = true, $useHtmlFooter = true, $useBanner = true, $useText = true, $useLinks = true, $useTable = true, $maxage = 0, $timeout = 10, $urlTarget = '_top')
    {
        //Default to the name in the url
        if (null == $name || '' == trim($name)) $name = array_pop(explode('/', $url));
        $this->handleDataDir(false, htmlentities("$name.$useHtmlHeader.$useHtmlFooter.$useBanner.$useText.$useLinks.$useTable.$maxage.$timeout.$urlTarget"));
        $yr_cached = $this->datapath;
        // Clean name
        $name = YRComms::convertEncodingUTF($name);
        $name = YRComms::convertEncodingEntities($name);
        // Clean URL
        $url = YRComms::convertEncodingUTF($url);
        // Is intermediate storage enabled, and do we really need to load new data, or keep intermediate storage data?
        if (($maxage > 0) && ((file_exists($yr_cached)) && ((time() - filemtime($yr_cached)) < $maxage))) {
            $data['value'] = file_get_contents($yr_cached);
            // Check for errors
            if (false == $data['value']) {
                $data['value'] = '<p>Det oppstod en feil mens værdata ble lest fra lokalt mellomlager. Vennligst gjør administrator oppmerksom på dette! Teknisk: Sjekk at rettighetene er i orden som beskrevet i bruksanvisningen for dette scriptet</p>';
                $data['error'] = true;
            }
        }
        // We run live, and at the same time save a version for intermediate storage
        else {
            $data = $this->generateHTML($url, $name, $xml->getXMLTree($url, $try_curl, $timeout), $useHtmlHeader, $useHtmlFooter, $useBanner, $useText, $useLinks, $useTable, $urlTarget);
            // Save to intermediate storage
            if ($maxage > 0 && !$data['error']) {
                $f = fopen($yr_cached, "w");
                if (null != $f) {
                    fwrite($f, $data['value']);
                    fclose($f);
                }
            }
        }
        // Return result
        return $data['value'];
    }

    private function getErrorMessage()
    {
        if (isset($this->yr_data['ERROR'])) {
            $error = $this->yr_data['ERROR'][0]['VALUE'];
            //die(retar($error));
            $this->ht .= '<p style="color:red; background:black; font-weight:900px">' . $error . '</p>';
            return true;
        }
        return false;
    }

    //Main
    public function generateHTML($url, $name, $data, $useHtmlHeader = true, $useHtmlFooter = true, $useBanner = true, $useText = true, $useLinks = true, $useTable = true, $urlTarget = '_top')
    {
        // Fill in data from the parameters
        $this->ht = '';
        $this->yr_url = $url;
        $this->yr_name = $name;
        $this->yr_data = $data;

        // Generate HTML in $ht
        $this->getHeader($useHtmlHeader);
        $data['error'] = $this->getErrorMessage();
        if ($useBanner) $this->getBanner($urlTarget);
        $this->getCopyright($urlTarget);
        if ($useText) $this->getWeatherText();
        if ($useLinks) $this->getLinks($urlTarget);
        if ($useTable) {
            $this->getWeatherTableHeader();
            $this->getWeatherTableContent();
            $this->getWeatherTableFooter($urlTarget);
        }
        $this->getFooter($useHtmlFooter);

        // Return result
        //return YRComms::convertEncodingEntities($this->ht);
        $data['value'] = $this->ht;
        return $data;
    }
}
