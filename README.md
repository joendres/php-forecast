# Weather forecast in PHP format

We've created a PHP script that can be included on your website.

PHP is a widely used scripting language, and most web hosting and cloud providers offer PHP support.

## How to do it
1. You must have access to a web server with PHP version 5 or later.
2. [Download the script (zip file)] (https://github.com/YR/php-forecast/archive/master.zip) and extract the files to a folder on your machine. You can also clone this repo if you use Git.
3. Edit the file yr.php with a text editor (such as Notepad). At the top of yr.php you will find instructions on what to edit.
4. Use an FTP program, or Git, to upload the file to your server (if you need help with your web host or cloud provider).
5. Go to http: //www.yourdomain/yr.php. Here you will now be able to see the alert.

You can also copy the PHP code in this script and paste it into another page (included in your design). Make sure you change the settings in yr.php accordingly.

You need to change the script if you can PHP, but remember that you must follow [the terms of use of data from yr.no] (http://om.yr.no/verdata/vilkar/) (you must Have a link to yr.no well visible on your site).

** Remember that you must always use the latest version of the script. **

Good luck!

## Important!
* If changes are made to yr.no that affect how the PHP alert works, you will need to download an updated version here. To get notifications about updates, you can use the "Watch" feature of GitHub.
* Read the terms for use of data from yr.no on [terms for use of data from yr.no] (http://om.yr.no/verdata/vilkar/).
* yr.no is unfortunately unable to provide support or help you implement the PHP script on your web site. If you do not get the script to work, you need help from someone who knows PHP and easy server administration.
* If you are further developing the script (removing any errors or adding new features), yr.no will gladly share what you have done with other users of yr.php! If you want to share what you have done with others, send an email to yr (krøllalfa) met.no.
 
## Credits
* The first version of the script was created by Øyvind Skau, anubix.net. The script was later developed and improved by Lennart André Rolland, with more in NRK.
* The script is inspired by XML Parser Class / Eric Rosebrock, phpfreaks.com.
* Classes are originally from kris@h3x.com, devdump.com/phpxml.php
